![Logo.png](https://bitbucket.org/repo/MrAz9Lz/images/2265690389-Logo.png)

# Floating Action Suite
[![](https://jitpack.io/v/org.bitbucket.AppSpireLLC/floating-action-suite.svg)](https://jitpack.io/#org.bitbucket.AppSpireLLC/floating-action-suite)

### Further Development will be handled by App-Spire LLC
Yet another implementation of [Floating Action Button](http://www.google.com/design/spec/components/buttons.html#buttons-floating-action-button) for Android with lots of features.

## Add To Project
* **Step 1** Add it in your root build.gradle at the end of repositories:  
![Screen Shot 2018-08-09 at 10.23.43 AM.png](https://bitbucket.org/repo/MrAz9Lz/images/1239335425-Screen%20Shot%202018-08-09%20at%2010.23.43%20AM.png)   

* **Step 2** Add the dependency.
![Screen Shot 2018-08-09 at 10.25.16 AM.png](https://bitbucket.org/repo/MrAz9Lz/images/1033564503-Screen%20Shot%202018-08-09%20at%2010.25.16%20AM.png)
# Requirements
The library requires minimum Android **API Level 14+**.

# Demo


# Screenshots
![Main screen](/screenshots/main_screen.webp) ![Menu closed](/screenshots/menu_closed.webp) ![Menu default opened](/screenshots/menu_default_opened.webp) ![Menu custom opened](/screenshots/menu_custom_opened.webp) ![Menu mini opened](/screenshots/menu_mini_opened.webp) ![Menu right opened](/screenshots/menu_right_opened.webp) ![Menu down opened](/screenshots/menu_down_opened.webp) ![Progress background](/screenshots/progress_background.webp) ![Progress no background](/screenshots/progress_no_background.webp)

# Features
- Ripple effect on Android Lollipop devices
- Option to set custom **normal**/**pressed**/**ripple** colors
- Option to set custom shadow color and offsets
- Option to disable shadow for buttons and (or) labels
- Option to set custom animations
- Option to set custom icon drawable
- Support for **normal** `56dp` and **mini** `40dp` button sizes
- Custom FloatingActionMenu icon animations
- Option to expand menu up and down
- Option to show labels to the left and to the right of the menu
- Option to show circle progress on `FloactinActionButton`
- Option to add button to the `FloatingActionMenu` programmatically
- Option to dim the `FloatinActionMenu`'s background
- *Option to remove all buttons from the `FloatingActionMenu`*
- *Option to set a label for the `FloatingActionMenu`'s button*

# Usage

Add the `com.github.clans.fab.FloatingActionButton` to your layout XML file.
```XML
<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:fab="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <ListView
        android:id="@+id/list"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />

    <com.github.clans.fab.FloatingActionButton
        android:id="@+id/fab"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="bottom|end"
        android:layout_marginBottom="8dp"
        android:layout_marginRight="8dp"
        android:layout_marginEnd="8dp"
        android:src="@drawable/ic_menu"
        fab:fab_colorNormal="@color/app_primary"
        fab:fab_colorPressed="@color/app_primary_pressed"
        fab:fab_colorRipple="@color/app_ripple"/>

</FrameLayout>
```
You can set an icon for the **FloatingActionButton** using `android:src` xml attribute. Use drawables of size `24dp` as specified by [guidlines](http://www.google.com/design/spec/components/buttons.html#buttons-floating-action-button). Icons of desired size can be generated with [Android Asset Studio](http://romannurik.github.io/AndroidAssetStudio/icons-generic.html).

### Floating action button
Here are all the **FloatingActionButton**'s xml attributes with their **default values** which means that you don't have to set all of them:
```XML
<com.github.clans.fab.FloatingActionButton
        android:id="@+id/fab"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="bottom|end"
        android:layout_marginBottom="8dp"
        android:layout_marginRight="8dp"
        android:layout_marginEnd="8dp"
        android:src="@drawable/your_icon_drawable"
        app:fab_colorNormal="#DA4336"
        app:fab_colorPressed="#E75043"
        app:fab_colorRipple="#99FFFFFF"
        app:fab_showShadow="true"
        app:fab_shadowColor="#66000000"
        app:fab_shadowRadius="4dp"
        app:fab_shadowXOffset="1dp"
        app:fab_shadowYOffset="3dp"
        app:fab_size="normal"
        app:fab_showAnimation="@anim/fab_scale_up"
        app:fab_hideAnimation="@anim/fab_scale_down"
        app:fab_label=""
        app:fab_progress_color="#FF009688"
        app:fab_progress_backgroundColor="#4D000000"
        app:fab_progress_indeterminate="false"
        app:fab_progress_max="100"
        app:fab_progress="0"
        app:fab_progress_showBackground="true"/>
```
All of these **FloatingActionButton**'s attributes has their corresponding getters and setters. So you can set them **programmatically**.

### Floating action menu
Here are all the **FloatingActionMenu**'s xml attributes with their **default values** which means that you don't have to set all of them:
```XML
<com.github.clans.fab.FloatingActionMenu
        android:id="@+id/menu"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentBottom="true"
        android:layout_alignParentRight="true"
        android:layout_alignParentEnd="true"
        android:layout_marginRight="10dp"
        android:layout_marginEnd="10dp"
        android:layout_marginBottom="10dp"
        android:layout_marginLeft="10dp"
        android:layout_marginStart="10dp"
        fab:menu_fab_size="normal"
        fab:menu_showShadow="true"
        fab:menu_shadowColor="#66000000"
        fab:menu_shadowRadius="4dp"
        fab:menu_shadowXOffset="1dp"
        fab:menu_shadowYOffset="3dp"
        fab:menu_colorNormal="#DA4336"
        fab:menu_colorPressed="#E75043"
        fab:menu_colorRipple="#99FFFFFF"
        fab:menu_animationDelayPerItem="50"
        fab:menu_icon="@drawable/fab_add"
        fab:menu_buttonSpacing="0dp"
        fab:menu_labels_margin="0dp"
        fab:menu_labels_showAnimation="@anim/fab_slide_in_from_right"
        fab:menu_labels_hideAnimation="@anim/fab_slide_out_to_right"
        fab:menu_labels_paddingTop="4dp"
        fab:menu_labels_paddingRight="8dp"
        fab:menu_labels_paddingEnd="8dp"
        fab:menu_labels_paddingBottom="4dp"
        fab:menu_labels_paddingLeft="8dp"
        fab:menu_labels_paddingStart="8dp"
        fab:menu_labels_padding="8dp"
        fab:menu_labels_textColor="#FFFFFF"
        fab:menu_labels_textSize="14sp"
        fab:menu_labels_cornerRadius="3dp"
        fab:menu_labels_colorNormal="#333333"
        fab:menu_labels_colorPressed="#444444"
        fab:menu_labels_colorRipple="#66FFFFFF"
        fab:menu_labels_showShadow="true"
        fab:menu_labels_singleLine="false"
        fab:menu_labels_ellipsize="none"
        fab:menu_labels_maxLines="-1"
        fab:menu_labels_style="@style/YourCustomLabelsStyle"
        fab:menu_labels_position="left"
        fab:menu_openDirection="up"
        fab:menu_backgroundColor="@android:color/transparent"
        fab:menu_fab_label="your_label_here"
        fab:menu_fab_show_animation="@anim/my_show_animation"
        fab:menu_fab_hide_animation="@anim/my_hide_animation">

        <com.github.clans.fab.FloatingActionButton
            android:id="@+id/menu_item"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:src="@drawable/ic_star"
            fab:fab_size="mini"
            fab:fab_label="Menu item 1" />

    </com.github.clans.fab.FloatingActionMenu>
```

If you're using custom style for labels - other labels attributes will be ignored.

Labels shadow preferences depends on their corresponding **FloatingActionButtons**' shadow preferences.

For more usage examples check the **sample** project.

# Changelog

# Credits

Work continued from [Clans/FloatingActionButton](https://github.com/Clans/FloatingActionButton).

# License
MIT License

Copyright (c) 2018 App-Spire LLC (https://app-spire.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.